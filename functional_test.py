from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest

class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Edith has heard about a cool new online to-do app. She goes
        # to check out its homepage
        self.browser.get('http://localhost:8000')

        # She notices the page title and header mention to-do lists
        self.assertIn('Post IT!', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text  
        self.assertIn('Post IT!', header_text)

        # She is invited to enter a to-do item straight away
        inputbox = self.browser.find_element_by_id('id_new_post')  
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a Post IT!'
        )

        username = self.browser.find_element_by_id('post_it_user')
        username.send_keys('Lucky')

        # She types "Buy peacock feathers" into a text box (Edith's hobby
        # is tying fly-fishing lures)
        inputbox.send_keys('A new forum post')  
        inputbox.submit()

        # When she hits enter, the page updates, and now the page lists
        # "1: Buy peacock feathers" as an item in a to-do list table
        submit_button = self.browser.find_element_by_id('submit_post')
        submit_button.click()
        time.sleep(10)

        yesbox = self.browser.find_element_by_id('YES')
        yesbox.send_keys('YES')
        submitconfirm = self.browser.find_element_by_id('submit-confirm')
        submitconfirm.click()
        time.sleep(10)

        self.check_for_row_in_list_table('A new forum post')
        self.check_for_row_in_list_table('"Lucky" Posted :')

        # There is still a text box inviting her to add another item. She
        # enters "Use peacock feathers to make a fly" (Edith is very
        # methodical)
        # self.fail('Finish the test!') 

		#Disana, mahasiswa sebelum mengepost harus memasukkan nama mereka terlebih dahulu 
		#Setelah nama telah dimasukkan, barulah mereka boleh memasuki pesan tertentu sesuai topik.
		#Nah, setelah selesai posting mereka akan langsung masuk ke website!


if __name__ == '__main__':  
	unittest.main(warnings='ignore')  


# browser = webdriver.Firefox()

# #Karena PJJ, semua mahasiswa harus melakukan kegiatan kuliah secara online.
# #Namun, karena Line dan WA dianggap tidak mencukupi, maka mahasiswa disuruh mengakses sebuah website forum.
# #Forum tersebut ada di [insert nameh here later]
# browser.get('http://localhost:8000')

# #Nah, setelah memasuki link, mahasiswa dapat melihat bahwa judul dari website tersebut adalah forum
# assert 'forum' in browser.title, "Browster title was " + browser.title

# #Disana, mahasiswa sebelum mengepost harus memasukkan nama mereka terlebih dahulu
# #Setelah nama telah dimasukkan, barulah mereka boleh memasuki pesan tertentu sesuai topik.
# #Nah, setelah selesai posting mereka akan langsung masuk ke website!

# browser.quit()