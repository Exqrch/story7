from django.urls import resolve
from django.test import TestCase, Client
from .views import home, confirm, correct  
from .models import Post
from django.http import HttpRequest


# Create your tests here.
class UnitTest(TestCase):

	def test_function_used_correct(self) :
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_function_used_confirmation_page_correct(self):
		found = resolve('/confirm/')
		self.assertEqual(found.func, confirm)

	def test_function_used_correction_page_correct(self):
		found = resolve('/error/')
		self.assertEqual(found.func, correct)

	def test_home_return_correct_html(self):
		#Hasil dalam bentuk 0 dan 1
		response = self.client.get('/')
		# #Decode agar menjadi string kode HTML
		# html = response.content.decode('utf8')
		# #cek apakah benar kode html  
		# self.assertTrue(html.startswith('<html>'))
		# #Cek apakah title sudah benar  
		# self.assertIn('<title>Forum</title>', html)  
		# self.assertTrue(html.endswith('</html>'))  
		self.assertTemplateUsed(response, 'home.html')

	def test_confirm_return_correct_html(self):
		Post.objects.create(username="Admin", message="Something-Something")
		response = self.client.get('/confirm/')
		self.assertTemplateUsed(response, 'confirm.html')

	def test_error_return_correct_html(self):
		response = self.client.get('/error/')
		self.assertTemplateUsed(response, 'error.html')

	def test_confirm_can_delete_POST_request(self):
		Post.objects.create(username="Tobe Deleted", message="Delete me from existence")
		response = self.client.post('/confirm/', data={'YES' : 'bla'})
		self.assertEqual(Post.objects.all().count(), 0)

	def test_can_save_a_POST_request(self):
		response = self.client.post('/', data={'forum_post': 'A new forum post', 'username_input' : 'Lucky Susanto'})
		self.assertEqual(Post.objects.all().count(), 1)

	def test_can_save_a_Post(self):
		first_post = Post()
		first_post.username = "Lucky Susanto"
		first_post.message = "A new forum post"
		first_post.save()

		Post.objects.create(username="Anon", message="Random")

		saved_post = Post.objects.all()
		second_post = saved_post[1].message
		self.assertIn(second_post, 'Random')
		self.assertEqual(saved_post.count(), 2)

	def test_empty_post_not_saved(self):
		self.assertEqual(Post.objects.all().count(), 0)
