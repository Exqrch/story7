from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Post

# Create your views here.
def home(request):

	if (request.method == "POST"):
		if (request.POST['username_input'] != '' and request.POST['forum_post'] != ''):
			Post.objects.create(username=request.POST['username_input'], message=request.POST['forum_post'])
			return redirect('/confirm/')
		else:
			return redirect('/error/')
	if (len(Post.objects.all()) == 0):
		Post.objects.create(username="Admin", message="This is the start of the forum! Be the first one to post!")
	context = {'obj' : Post.objects.all()}
	return render(request, 'home.html', context)

def confirm(request):
	if (request.method == "POST"):
		if (request.POST['YES'] == 'YES'):
			return redirect('/')
		else:
			idx = len(Post.objects.all())-1
			post = Post.objects.all()
			post[idx].delete()
			return redirect('/')
	context = Post.objects.all()
	context = len(context)-1
	context = {'obj' : Post.objects.all()[context]}
	return render(request, 'confirm.html', context)

def correct(request):
	return render(request, 'error.html')